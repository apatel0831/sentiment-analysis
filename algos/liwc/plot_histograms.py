
import sys
sys.path.append(".")
import pymongo
from pymongo import MongoClient
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from multiprocessing import Pool, Process

def generateHist(category):
	client = MongoClient('prototype.aaditpatel.com', 27017) #use prototype.aaditpatel.com if not running on server
	db = client.mothering_data
	db.authenticate('sentiment-analysis','b!gd4+4')
	#liwc results
	liwc = db.liwc_results

	c = liwc.find({"Liwc."+category: {"$lte":100}},{"Liwc."+category:1, "_id":0})
	x = list(e["Liwc"][category] for e in c)

	num_bins = 100
	# the histogram of the data
	# add a 'best fit' line
	#y = mlab.normpdf(bins, mu, sigma)
	#plt.plot(bins, y, 'r--')	
	fig = plt.figure()
	ax1 = fig.add_subplot(111)
	ax1.set_xlabel('Score')
	ax1.set_ylabel('Relative Frequency')
	ax1.set_title('Histogram of %s' % category)
	ax1.hist(x, num_bins, normed=1, facecolor='green', alpha=0.5)

	# Tweak spacing to prevent clipping of ylabel
	#plt.subplots_adjust(left=0.15)
	ax1.set_xlim([0,50])
	ax1.set_ybound([0,0.6])
	fig.savefig('graphs/histo_%s.png' % (category), bbox_inches='tight')
	print "Saved histo_%s.png" % (category)


def generateScatter(category_tuple):
	client = MongoClient('prototype.aaditpatel.com', 27017) #use prototype.aaditpatel.com if not running on server
	db = client.mothering_data
	db.authenticate('sentiment-analysis','b!gd4+4')
	#liwc results
	liwc = db.liwc_results
	
	cat_1 = category_tuple[0]
	cat_2 = category_tuple[1]

	print cat_1
	print cat_2

	c = liwc.find({"Liwc."+cat_1:{"$lte":100, "$gt":-1}, "Liwc."+cat_2:{"$lte":100, "$gt":-1}},{"Liwc."+cat_1:1, "Liwc."+cat_2:1,"Liwc.posemo":1, "Liwc.negemo":1, "WordCount":1, "_id":0})
	#c.limit(1000)
	net = list(e["Liwc"]["posemo"] - e["Liwc"]["negemo"] for e in c)
	c.rewind()
	y = list(e["Liwc"][cat_1] for e in c)


	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	ax1.scatter(net,y)
	ax1.set_xlabel('Net Emotion')
	ax1.set_ylabel('%s score' % cat_1)
	ax1.set_title('%s vs Net Emotion' % (cat_1))

	# Tweak spacing to prevent clipping of ylabel
	#ax1.subplots_adjust(left=0.15)
	#ax1.set_xlim(left = 0)
	ax1.set_ylim(bottom = 0)
	fig.savefig('graphs/scatter_%s_v_net_emo.png' % (cat_1), bbox_inches='tight')
	print "Saved scatter_%s_v_net_emo.png" % (cat_1)




#scatter_plot[('insight','positive emotion'), ('insight','negative_emotion')...]

categories = ["posemo", "negemo","insight", "cause","discrep","tentat","certain","inhib","incl","excl","anx","anger","sad","health","money","relig","death","family"] #posemotion, negative emotion, subcategores for affective processes, cognitive processes, health, money, religion, death
#categories = ["family"]
category_tuples = [("insight", "anger"), ("anger","discrep"),("tentat","posemo"), ("certain","certain"),("sad","certain")]

if __name__ == "__main__":
	hist_pool = Pool(processes=31#len(categories) if len(categories) < 7 else 7)# start 4 worker processes
	scatter_pool = Pool(processes=4)#len(category_tuples))

	scatters = scatter_pool.map_async(generateScatter, category_tuples,1)
	scatters.get()

	hists = hist_pool.map_async(generateHist, categories, 1)
	hists.get()











