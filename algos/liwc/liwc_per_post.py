__author__ = 'Aravindan'

import sys
sys.path.append(".")
import pymongo
import MyLIWC
from collections import Counter
from pymongo import MongoClient

client = MongoClient('localhost', 27017) #use prototype.aaditpatel.com if not running on server
db = client.mothering_data
db.authenticate('sentiment-analysis','b!gd4+4')

#get the users collection
users_collection = db.users

#get the threads collection
threads_collection = db.threads

results_collection = db.liwc_results
results_collection.drop()

#get all threads
threads = threads_collection.find() #returns a cursor object c that we can iterate over

#initialize LIWC module - loads the dictionary and the list of all categories
MyLIWC.init()

#iterate over all threads
#for thread in c:
    #print all thread Ids out into console. Might take a while since it only batch process like 20 entries at once

    #get the size of the user collection
print 'Threads : ' + str(threads_collection.count())
print 'Users : ' + str(users_collection.count())

category_list = MyLIWC.header()

bulk = []

for thread in threads:
    posts_List = thread["Posts"]
    #results_collection.find({"ThreadId" : thread["ThreadId"]}).limit(1)
    for index, item in enumerate(posts_List):
        post = item["Text"]
        words = post.split()
        wordCountCounter = Counter(words)
        #print sum(wordCountCounter.values())
        wordCount = sum(wordCountCounter.values())
        #print(post)
        lst = MyLIWC.countcat(post,"dict")
        post_entry = {}
        liwc_entry = {}
        post_entry = {

            "ThreadId" : thread["ThreadId"] ,
            "ThreadTitle" : thread["ThreadTitle"] ,
            "PostId" : item["PostId"] ,
            "Post" : item["Text"] ,
            "WordCount" : wordCount
        }

        for cat in category_list:
            if cat in lst:
                liwc_entry[cat] = lst[cat] * ( 100.0 / wordCount )
            else:
                liwc_entry[cat] = 0.0

        #print liwc_entry
        post_entry["Liwc"] = liwc_entry

        bulk.append(post_entry);
        #print("insert")

results_collection.insert(bulk)