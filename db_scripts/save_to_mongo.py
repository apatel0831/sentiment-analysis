
import pymongo
import time
import datetime
import re #regular expression

from pymongo import MongoClient

def interact():
	import code
	code.InteractiveConsole(locals=globals()).interact()

print "Opening database and setting up tables..."

client = MongoClient('localhost', 27017) #for cloud, use host: prototype.aaditpatel.com and add authentication

db = client.mothering_data;
#db.authenticate('sentiment-analysis','b!gd4+4')

users_collection = db.users
threads_collection = db.threads
threads_collection.remove()
users_collection.remove()


users_collection.ensure_index("Username")
users_collection.ensure_index("UserId", unique = True)

threads_collection.ensure_index("ThreadId", unique = True)


raw_data = open('../raw_data/MegTable2.txt','r')

print "Reading data from raw file...."

counter = 0
post_user_pattern  = re.compile('MetaData:post+\s#\d+\sof\s\d+\s*(.+)\s*Trader Feedback') #The regex to find the username of poster. The username can be set of characters, including special characters, at arbitrary length.
quote_user_pattern = re.compile('Quote:\s+Originally Posted by\s(.{1,}?\s+?.{1,}?)\s{1,}?') #regex to find username of quoted user

bulk_threads = []
user_map = {} #map between username and user object id, cached in memory. We need this because the quoted user only has a username, not a user_id associated with him. Pinging the database everytime is slow, so we do this in memory.
thread_map = {} #map that lets us know if a thread has already been accounted for 

for line in raw_data:

	counter += 1
	if counter % 100000 == 0:
		print "Don't worry, still running...."

	if line.startswith("PostId:"):
		postid = line.replace("PostId:", "")
		postid = int(postid)

	if line.startswith("ThreadId:"):
		threadid = line.replace("ThreadId:", "")
		threadid = int(threadid)

	if line.startswith("Time:"):
		time_string = line.replace("Time:", "")
		time_string = time_string.rstrip()	
		date_time = datetime.datetime.strptime(time_string,"%m/%d/%y at %I:%M%p")
		#time = time.mktime(d.timetuple()) 

	
	if line.startswith("Title:"):
		title = line.replace("Title:","",1)
		title = title.replace(";","")

	if line.startswith("UserId:"):
		userid = line.replace("UserId:","")
		userid.strip()
		userid = int(userid)

	if line.startswith("MetaData:"):

		userStartedThread = False

		if "Thread Starter" in line:
			line = line.replace("Thread Starter", "",1)
			userStartedThread = True

		metadata = line.replace(";", "",1)
		match = re.search(post_user_pattern, metadata) #precompiled pattern. Match to find group for username
		
		if match:
			username = match.group(1)
			username = username.strip()
			user_obj = {"Username": username, "UserId": userid}
			user_object_id = users_collection.find_and_modify({"UserId": userid}, user_obj, upsert = True, new = True)
			user_map[username] = user_object_id
			
			if userStartedThread and threadid not in thread_map: #user starting thread implies a new thread, create the thread object and append to thread list
				threadStarter = user_object_id
				
				thread = {
					"ThreadId": threadid,
					"ThreadTitle": title,
					"ThreadStarter": threadStarter,
					"DateTime": date_time,
					"Posts": []
				}

				bulk_threads.append(thread)
				thread_map[threadid] = True

		else:
			print "Error: Username could not be extracted"
			print "Line Number: " + str(counter) + " in raw text"
			exit()

	if line.startswith("Text:"):
		text = line.replace("Text::", "",1)
		text = line.replace("Text:", "",1)
		text = text.strip()

		if user_map[username] is None:
			interact()

		post = {
			"PostId" : postid,
			"UserObjectId": user_map[username],
     		"Text" : text,
			"DateTime": date_time 
		}

		thread["Posts"].append(post)

	if line.startswith("Quote:Quote:"): #Quote:Quote: means the user has actually quoted someone

		quote = line.replace("Quote:","",1) #elminate first Quote:
		match = re.search(quote_user_pattern,quote) #search for the username of the quotee

		if match:
			m = match.group(1).strip() #strip leading/ending whitespaces
			m = m.rstrip('\xc2\xa0') #strip non-breaking space (this arises because of parsing between different encoding schemes)
			m = m.strip() #perform new whitespace strips
			quote_text = quote.replace(match.group(0),"").strip() #the quoted text is everything that is not the username and white spaces
			if m in user_map: #the quoted user should be in the user_map, but check anyway
				quotee_user_object_id = user_map[m] #get the object_id of the quoted_user
				post["Quote"] = {"QuoteText": quote_text, "QuoteeUserObjectId": quotee_user_object_id}


print "Inserting into database..."

threads_collection.insert(bulk_threads)
print "Thread count: " + str(len(bulk_threads))

raw_data.close()

