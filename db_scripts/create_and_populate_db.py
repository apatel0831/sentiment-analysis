import sqlite3


#connect to db file

conn = sqlite3.connect('../db/motherhood_data.db')
conn.text_factory = str
c = conn.cursor()

#create schema
c.execute(''' CREATE TABLE if not exists posts (threadid integer, subject text, body blob) ''')


raw_data = open('../raw/MotheringTable.txt','r')
posts = [] #empty list

for line in raw_data:
	if "ThreadId:" in line:
		thread = line.replace("ThreadId:", "")
	
	if "Subject:" in line:
		subject = line.replace("Subject:","")
		subject = subject.replace(";","")

	if "Body:" in line:
		body = line.replace("Body:", "")

		posts.append((thread,subject,body))


c.executemany('INSERT INTO posts VALUES (?,?,?)', posts)
conn.commit()
conn.close()


raw_data.close()